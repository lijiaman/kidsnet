<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width，initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="themes/red.min.css" />
    <link rel="stylesheet" href="themes/jquery.mobile.icons.min.css" />
    <!--<link type="text/css" rel="stylesheet" href="jquery.mobile-1.4.5.min.css">-->
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile.structure-1.4.5.min.css" />
    <link type="text/css" rel="stylesheet" href="style.css">
    <script src="jquery-2.2.0.min.js"></script>
    <script src="jquery.mobile-1.4.5.min.js"></script>
    <script>var userid = <?php echo $_GET["id"];?> ; </script>
    <script>
        var username;
        var headimgurl;
        $.ajax({
            url:"echo.php",
            type:"POST",
            data:{id:userid},
            success: function (data) {
                var obj = JSON.parse(data);
                username = obj.username;
                headimgurl = obj.headimgurl;
            },
            error:function(){

            }
        });
    </script>
</head>
<body>

<div data-role="page" id="page1" data-theme="b">

    <script>
        $(document).on("pagebeforecreate","#page1",function(){
            $.ajax({
                url:"getKidInfo.php",
                type:"POST",
                data:{id:userid},
                success: function (data) {
                    var obj = JSON.parse(data);
                    var kidname = obj.kidname;
                    var kidimgurl = obj.kidheadimg;
                    document.getElementById("topname").innerHTML=kidname;
                    $("#kidimg").attr("src",kidimgurl);
                },
                error:function(){

                }
            });
            var pics = new Array();

            $.ajax({
                url:"echoPage1.php",
                type:"POST",
                data:{id:userid},
                success: function (data) {
                    var obj = JSON.parse(data);
                    var len = obj.words.length;
                    pics = obj.pictures;
                    var show = "";
                    for(var i = 0; i < len; i++){
                        var start = '<li><img src="'+obj.userhead[i]+'" height="60" width="60">';
                        var mid = '<p>'+obj.words[i]+'</p>';
                        var pic = "";
                        for(var j = 0; j < pics[i].length; j++){
                            pic += '<img src="'+pics[i][j]+'" height="80" width="80">';
                        }
                        var end = '</li>';
                        show = start+mid+pic+end;
                        var list = document.getElementById("recordList");
                        list.innerHTML += show;
                    }
                    $('#recordList').listview('refresh');

                },
                error:function(){

                }
            });

            $.ajax({
                url:"panel.php",
                type:"POST",
                data:{id:userid},
                success:function(data){
                    var obj = JSON.parse(data);
                    var len = obj.kname.length;
                    var kidul= document.getElementById("kidlist");
                    kidul.innerHTML = "";
                    var mid = "";
                    for(var i = 0; i < len; i++){
                        mid = '<li id="'+obj.kID[i]+'"><a href="#" class="listBackground" >'+obj.kname[i]+'</a></li>';
                        kidul.innerHTML += mid;
                    }

                    $('#kidlist').listview('refresh');
                },
                error:function(){

                }

            });


        });

        $(document).ready(function(){

            $("#kidlist").on("click","li", function() {
                var kidid = $(this).attr("id");
//                        location.href='index.php?kidid='+kidid;
//                        location.href='#page3?kidid='+kidid;
                $.ajax({
                    url:"getSelectKid.php",
                    type:"POST",
                    data:{id:kidid},
                    success:function(data){
                        var obj = JSON.parse(data);
                        var len = obj.words.length;
                        var photos = new Array();
                        photos = obj.pictures;
                        var show = "";
                        for(var i = 0; i < len; i++){
                            var start = '<li><img src="'+obj.userhead[i]+'" height="60" width="60">';
                            var mid = '<p>'+obj.words[i]+'</p>';
                            var pic = "";
                            for(var j = 0; j < pics[i].length; j++){
                                pic += '<img src="'+pics[i][j]+'" height="80" width="80">';
                            }
                            var end = '</li>';
                            show = start+mid+pic+end;
                            var list = document.getElementById("recordList");
                            list.innerHTML += show;
                        }
                        $('#recordList').listview('refresh');

                    },
                    error:function(){
                        alert("error");
                    }

                });

            });
        });

    </script>

    <div data-role="panel" id="kids" data-position="left" data-display="overlay" class="listBackground">
        <ul data-role="listview" id="list">
            <li><a href="#page1-create" class="listBackground">创建孩子</a></li>
            <li><a href="#page1-invite" class="listBackground">加入孩子</a></li>
        </ul>
        <p></p>
        <ul data-role="listview" id="kidlist">

        </ul>



    </div>

    <div data-role="header" data-theme="a" data-position="fixed">
        <a href="#kids" class="ui-btn ui-shadow ui-corner-all ui-icon-left-kids ui-btn-icon-notext">选择孩子</a>
        <h1 id="topname">时光轴</h1>
        <a href="#page1-edit" class="ui-btn ui-shadow ui-corner-all ui-icon-right-edit ui-btn-icon-notext">记录</a>
    </div>
    <div class="imgcenter topBackground" width="100%">
        <img src="kid2.jpg" width="50px" height="50px" class="topPicture" id="kidimg">
    </div>
    <div data-role="content" style="position: absolute;width: 100%;height: -webkit-calc(100% - 44px * 2)">
        <div id="topname2"></div>
        <ul data-role="listview" data-inset="true" id="recordList">
            <!--            <li>-->
            <!--                  <img src="kid2.jpg" height="80" width="80">-->
            <!--                  <h2></h2>-->
            <!--                <p>好玩！！</p>-->
            <!--                <p>-->
            <!--                    <img src="kid2.jpg" height="80" width="80">-->
            <!--                    <img src="kid2.jpg" height="80" width="80">-->
            <!--                </p>-->
            <!--            </li>   -->

        </ul>
    </div>

    <div data-role="footer" data-position="fixed">
        <div data-role="navbar">
            <ul>
                <li><a href="#page1" class="ui-btn ui-btn-icon-top ui-icon-kid-paw">足迹</a></li>
                <li><a href="#page2" class="ui-btn ui-btn-icon-top ui-icon-kid-find-outline">动态</a></li>
                <li><a href="#page3" class="ui-btn ui-btn-icon-top ui-icon-kid-user-outline">我</a></li>
            </ul>
        </div>
    </div>
</div>

<div data-role="page" id="page1-create" data-theme="a">
    <?php
    //设置报错级别，忽略警告，设置字符
    error_reporting(E_ALL || ~E_NOTICE);
    header("Content-type:text/html; charset=utf-8");
    require_once "jssdkforsae.php";
    $jssdk = new JSSDK("wxdd0345e9c31b9a00", "d4624c36b6795d1d99dcf0547af5443d");
    $signPackage = $jssdk->GetSignPackage();
    ?>
    <div data-role="header">
        <a href="#page1" class="ui-btn ui-icon-back ui-corner-all ui-btn-icon-left ui-btn-icon-notext">返回</a>
        <h1>创建孩子</h1>
    </div>
    <div data-role="content">
        <img id="head" src="addphoto.png" width="64px" height="64px"></>
    <p></p>
    <form id="createform" name="createform">
        <label for="kidname">孩子名</label>
        <input type="text" name="kidname" id="kidname">
        <fieldset data-role="controlgroup">
            <legend>性别</legend>
            <label for="boy">男孩</label>
            <input type="radio" name="gender" id="boy" value="男">
            <label for="girl">女孩</label>
            <input type="radio" name="gender" id="girl" value="女">
        </fieldset>
        <label for="birth">出生日期</label>
        <input type="date" name="birth" id="birth">
        <input type="submit" id="createbtn" name="createbtn" value="完成"/>
    </form>
</div>
<script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script>
    wx.config({
        debug: true, //调试阶段建议开启
        appId: '<?php echo $signPackage["appId"];?>',
        timestamp: <?php echo $signPackage["timestamp"];?>,
        nonceStr: '<?php echo $signPackage["nonceStr"];?>',
        signature: '<?php echo $signPackage["signature"];?>',
        jsApiList: [
            /*
             * 所有要调用的 API 都要加到这个列表中
             * 这里以图像接口为例
             */
            "chooseImage",
            "previewImage",
            "uploadImage",
            "downloadImage",
        ]
    });

    //定义images用来保存选择的本地图片ID，和上传后的服务器图片ID
    var images = {
        localId: [],
        serverId: []
    };

    $("#head").on("tap",function(){
        wx.chooseImage ({
            success : function(res){
                images.localId = res.localIds;  //保存到images
                // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
                var imgsrc = images.localId[0];
                $("#head").attr("src",imgsrc);
            }
        });
    });

    $("#createbtn").on("tap",function(){
        var kname = $("#kidname").val();
        var ksex = $("input[name='gender']").val();
        var kbirth = $("#birth").val();
        var i = 0, len = images.localId.length;
        function wxUpload(){
            wx.uploadImage({
                localId: images.localId[i], // 需要上传的图片的本地ID，由chooseImage接口获得
                isShowProgressTips: 1, // 默认为1，显示进度提示
                success: function (res) {
                    i++;
                    //将上传成功后的serverId保存到serverid
                    images.serverId.push(res.serverId);
                    if(i < len){
                        wxUpload();
                    }else{
                        $.ajax({
                            url:"uploadhead.php",
                            type:"POST",
                            data:{
                                mediaID:images.serverId,
                                kidID:kidid,
                                userID:userid,
                                kidname:kname,
                                kidsex:ksex,
                                kidbirth:kbirth
                            },
                            success: function (data) {
                                location.href="index.php";
                            },
                            error:function(){
                            }
                        });
                    }
                }
            });
        }
        wxUpload();

    });

</script>
</div>

<div data-role="page" id="page1-invite">
    <div data-role="header">
        <a href="#page1" class="ui-btn ui-icon-back ui-corner-all ui-btn-icon-left ui-btn-icon-notext">返回</a>
        <h1>邀请家人</h1>
    </div>
    <div data-role="content">
        <label for="invitecode">请输入邀请码</label>
        <input type="text" name="invite-code" id="invite-code">
        <input type="submit" id="invite-btn" name="invite-btn" value="完成"/>
    </div>

    <script>
        $("#invite-btn").on("tap",function(){
            var code = $("#invite-code").val();
            $.ajax({
                type:"POST",
                cache:false,
                url:"invite.php",
                data:{parentid:userid,invitecode:code},
                success:function(data){
                    location.href = "index.php";
                },
                error:function(){

                }
            })
        });
    </script>
</div>

<div data-role="page" id="page1-edit">
    <?php
    //设置报错级别，忽略警告，设置字符
    error_reporting(E_ALL || ~E_NOTICE);
    header("Content-type:text/html; charset=utf-8");
    require_once "jssdkforsae.php";
    $jssdk = new JSSDK("wxdd0345e9c31b9a00", "d4624c36b6795d1d99dcf0547af5443d");
    $signPackage = $jssdk->GetSignPackage();
    ?>
    <div data-role="header" data-theme="a" data-position="fixed">
        <a href="#page1" class="ui-btn ui-shadow ui-corner-all ui-icon-back ui-btn-icon-notext">返回</a>
        <h1>发布日志</h1>
    </div>
    <div data-role="content">
        <form id="createday" name="createday">
            <textarea  name="words" autofocus rows="5" cols="20" wrap="physical" placeholder="宝贝此刻正在……" id="words"  required></textarea>
            <a id="location" href="#" class="ui-btn ui-shadow ui-corner-all ui-icon-location ui-btn-icon-notext">地理位置</a>
            <div id="place"></div>
            <br/>
            <img src="addphoto.png" id="upload">
            <p></p>
            <div id="photo"></div>
            <p></p>
            <ul data-role="listview" data-inset="true">
                <fieldset data-role="controlgroup">
                    <legend>发给谁看</legend>
                    <label for="public">公开</label>
                    <input type="radio" name="limit" id="public" value="公开">
                    <label for="harfpublic">半公开</label>
                    <input type="radio" name="limit" id="harfpublic" value="半公开">
                    <label for="private">私密</label>
                    <input type="radio" name="limit" id="private" value="私密">
                </fieldset>
                <li id="whichKids"><a href="#"><img src="icons/iconfont-shixin64.png" alt="OtherKids" class="ui-li-icon ui-corner-none">谁可以看</a></li>
                <ul data-role="listview" id = "chooseKids"></ul>
            </ul>
            <input type="submit" id="createi" name="createi" value="上传动态"/>
        </form>
    </div>

    <script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script charset="utf-8" src="http://map.qq.com/api/js?v=2.exp"></script>
    <script>
        wx.config({
            debug: false, //调试阶段建议开启
            appId: '<?php echo $signPackage["appId"];?>',
            timestamp: <?php echo $signPackage["timestamp"];?>,
            nonceStr: '<?php echo $signPackage["nonceStr"];?>',
            signature: '<?php echo $signPackage["signature"];?>',
            jsApiList: [
                /*
                 * 所有要调用的 API 都要加到这个列表中
                 * 这里以图像接口为例
                 */
                "chooseImage",
                "previewImage",
                "uploadImage",
                "downloadImage",
                "openLocation",
                "getLocation"
            ]
        });

        //定义images用来保存选择的本地图片ID，和上传后的服务器图片ID
        var images = {
            localId: [],
            serverId: []
        };

        wx.ready(function () {
            // 在这里调用 API
            $("#upload").on("tap",function(){
                wx.chooseImage ({
                    success : function(res){
                        images.localId = res.localIds;  //保存到images
                        // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
                        var imgview = "";
                        for(var i in images.localId){
                            var imgsrc = images.localId[i];
                            imgview = imgview + '<img width="64px" height="80px" src="'+imgsrc+'"/>&nbsp;';

                        }
                        $("#photo").append(imgview);
                    }
                });
            });
        });
        var lati;
        var longi;
        var speed;
        var accur;
        var Place;
        var kisid = 0;



        $("#chooseKids").on("click","li",function(){
            kisid = $(this).attr("id");
            //var kidid = $(this).attr("id");
        });

        $("#location").on("tap",function(){

            wx.getLocation({
                success: function(res) {
                    lati = res.latitude;
                    longi = res.longitude;
                    speed = res.speed;
                    accur = res.accuracy;

                    var geocoder = new qq.maps.Geocoder({
                        complete: function(result){
                            Place = result.detail.address;
                            $("#place").append(Place);
                        }
                    })
                    var latLng = new qq.maps.LatLng(lati,longi);
                    geocoder.getAddress(latLng);
                },
                cancel: function(res){
                    alert('用户拒绝授权获取地理位置');
                }

            });
        });

        $("#createi").on("tap",function(){
            var i = 0, len = images.localId.length;
            var word = $("#words").val();
            var pri = $("input[name='limit'][type='radio']:checked").val();

            function wxUpload(){
                //var formday = $("createday").serialize();
                wx.uploadImage({
                    localId: images.localId[i], // 需要上传的图片的本地ID，由chooseImage接口获得
                    isShowProgressTips: 1, // 默认为1，显示进度提示
                    success: function (res) {
                        i++;
                        //将上传成功后的serverId保存到serverid
                        images.serverId.push(res.serverId);
                        if(i<len){
                            wxUpload();
                        }else{
                            $.ajax({
                                url:"uploadimg.php",
                                type:"POST",
                                data:{mediaID:images.serverId,//图片
                                    parentID:userid,
                                    length:len,
                                    words:word,//文字
                                    limit:pri,//谁可以看
                                    place:Place,//地理位置
                                    latitude:lati,
                                    longitude:longi,
                                    kidsId:kisid
                                },
                                success: function (data) {
                                    location.href = '#page1';
                                },
                                error:function(){

                                }
                            });
                        }

                    }
                });
            }
            wxUpload();
        });

    </script>
</div>


<div data-role="page" id="page2" data-theme="b">
    <?php
    //设置报错级别，忽略警告，设置字符
    error_reporting(E_ALL || ~E_NOTICE);
    header("Content-type:text/html; charset=utf-8");
    require_once "jssdkforsae.php";
    $jssdk = new JSSDK("wxdd0345e9c31b9a00", "d4624c36b6795d1d99dcf0547af5443d");
    $signPackage = $jssdk->GetSignPackage();
    ?>
    <div data-role="header" data-theme="a">
        <div data-role="navbar">
            <ul>
                <li><a href="#recommend" >推荐</a></li>
                <li><a href="#care">关注</a></li>
                <li><a href="#page2" class="ui-btn-active ui-state-persist">发现</a></li>
            </ul>
        </div>
    </div>

    <div class="ui-content" data-role="content" style="position: absolute;width: 100%;height: -webkit-calc(100% - 44px * 2)">
        <div id="myPlace">正在：</div>
        <p></p>
        <br/>
        <ul data-role="listview"  data-inset="true" id="nearByL">

        </ul>
    </div>

    <div data-role="footer" data-position="fixed">
        <div data-role="navbar">
            <ul>
                <li><a href="#page1" class="ui-btn ui-btn-icon-top ui-icon-kid-paw-outline">足迹</a></li>
                <li><a href="#page2" class="ui-btn ui-btn-icon-top ui-icon-kid-find">动态</a></li>
                <li><a href="#page3" class="ui-btn ui-btn-icon-top ui-icon-kid-user-outline">我</a></li>
            </ul>
        </div>
    </div>
    <script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script charset="utf-8" src="http://map.qq.com/api/js?v=2.exp"></script>
    <script>
        wx.config({
            debug: false, //调试阶段建议开启
            appId: '<?php echo $signPackage["appId"];?>',
            timestamp: <?php echo $signPackage["timestamp"];?>,
            nonceStr: '<?php echo $signPackage["nonceStr"];?>',
            signature: '<?php echo $signPackage["signature"];?>',
            jsApiList: [
                "openLocation",
                "getLocation"
            ]
        });
        var myLatitude;
        var myLongitude;
        var myLocation;

        $(document).on("pagebeforecreate","#page2",function(){
            wx.getLocation({
                success: function(res) {
                    myLatitude = res.latitude;
                    myLongitude = res.longitude;

                    var geocoder = new qq.maps.Geocoder({
                        complete: function(result){
                            myLocation = result.detail.address;
                           // alert(myLocation);
                            $("#myPlace").append(myLocation);
                        }
                    })
                    var latLng = new qq.maps.LatLng(myLatitude,myLongitude);
                    geocoder.getAddress(latLng);
                },
                cancel: function(res){
                    alert('用户拒绝授权获取地理位置');
                }
            });

            var pics = new Array();
            $.ajax({
                url:"nearbyRecords.php",
                type:"POST",
                data:{
                    id:userid,
                    latitude:myLatitude,
                    longitude:myLongitude
                },
                success: function(data){
                    var obj = JSON.parse(data);
                    // alert(obj.parentname[0]);
                    var len = obj.words.length;
                    pics = obj.pictures;
                    var show = "";
                    for(var i = 0; i < len; i++){
                        //alert(obj.words[i]);
                        var start = '<li><img src="'+obj.userhead[i]+'" height="60" width="60">';
                        var mid = '<p>'+obj.words[i]+'</p>';
                        var pic = "";
                        for(var j = 0; j < pics[i].length; j++){
                            pic += '<img src="'+pics[i][j]+'" height="80" width="80">';
                        }/**/
                        var end = '</li>';
                        show = start+mid+pic+end;
                        //show = start+mid+end;
                        var list = document.getElementById("nearByL");
                        list.innerHTML += show;
                    }
                    $('#nearByL').listview('refresh');
                },
                error:function(){

                }
            });

        });
    </script>
</div>

<div data-role="page" id="recommend">
    <?php
    //设置报错级别，忽略警告，设置字符
    error_reporting(E_ALL || ~E_NOTICE);
    header("Content-type:text/html; charset=utf-8");
    require_once "jssdkforsae.php";
    $jssdk = new JSSDK("wxdd0345e9c31b9a00", "d4624c36b6795d1d99dcf0547af5443d");
    $signPackage = $jssdk->GetSignPackage();
    ?>
    <div data-role="header" data-theme="a">
        <div data-role="navbar">
            <ul>
                <li><a href="#recommend" class="ui-btn-active ui-state-persist">推荐</a></li>
                <li><a href="#care">关注</a></li>
                <li><a href="#page2">发现</a></li>
            </ul>
        </div>
    </div>

    <div class="ui-content" data-role="content" style="position: absolute;width: 100%;height: -webkit-calc(100% - 44px * 2)">
        <br/>
        <div id="lTest"></div>
        <ul data-role="listview"  data-inset="true" id="recommendRecords">

        </ul>
    </div>

    <div data-role="footer" data-position="fixed">
        <div data-role="navbar">
            <ul>
                <li><a href="#page1" class="ui-btn ui-btn-icon-top ui-icon-kid-paw-outline">足迹</a></li>
                <li><a href="#page2" class="ui-btn ui-btn-icon-top ui-icon-kid-find">动态</a></li>
                <li><a href="#page3" class="ui-btn ui-btn-icon-top ui-icon-kid-user-outline">我</a></li>
            </ul>
        </div>
    </div>
    <script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script charset="utf-8" src="http://map.qq.com/api/js?v=2.exp"></script>
    <script>
        $(document).on("pagebeforecreate","#recommend",function(){
            var pics = new Array();
            $("#lTest").append("ok");
            $.ajax({
                url:"recommendRecords.php",
                type:"POST",
                data:{
                    id:userid
                },
                success: function(data){
                   var obj = JSON.parse(data);
                    // alert(obj.parentname[0]);
                    var len = obj.words.length;
                    pics = obj.pictures;
                    var show = "";
                    for(var i = 0; i < len; i++){
                        //alert(obj.words[i]);
                        var start = '<li><img src="'+obj.userhead[i]+'" height="60" width="60">';
                        var mid = '<p>'+obj.words[i]+'</p>';
                        var pic = "";
                        for(var j = 0; j < pics[i].length; j++){
                            pic += '<img src="'+pics[i][j]+'" height="80" width="80">';
                        }
                        var end = '</li>';
                        show = start+mid+pic+end;
                        //show = start+mid+end;
                        var list = document.getElementById("recommendRecords");
                        list.innerHTML += show;
                    }
                    $('#recommendRecords').listview('refresh');
                },
                error:function(){

                }
            });

        });
    </script>
</div>

<div data-role="page" id="care">
    <?php
    //设置报错级别，忽略警告，设置字符
    error_reporting(E_ALL || ~E_NOTICE);
    header("Content-type:text/html; charset=utf-8");
    require_once "jssdkforsae.php";
    $jssdk = new JSSDK("wxdd0345e9c31b9a00", "d4624c36b6795d1d99dcf0547af5443d");
    $signPackage = $jssdk->GetSignPackage();
    ?>
    <div data-role="header" data-theme="a">
        <div data-role="navbar">
            <ul>
                <li><a href="#recommend">推荐</a></li>
                <li><a href="#care" class="ui-btn-active ui-state-persist">关注</a></li>
                <li><a href="#page2">发现</a></li>
            </ul>
        </div>
    </div>
    <div class="ui-content" data-role="content" style="position: absolute;width: 100%;height: -webkit-calc(100% - 44px * 2)">
        <div id="lTest2">正在：</div>
        <p></p>
        <br/>
        <ul data-role="listview"  data-inset="true" id="careRecords">

        </ul>
    </div>
    <div data-role="footer" data-position="fixed">
        <div data-role="navbar">
            <ul>
                <li><a href="#page1" class="ui-btn ui-btn-icon-top ui-icon-kid-paw-outline">足迹</a></li>
                <li><a href="#page2" class="ui-btn ui-btn-icon-top ui-icon-kid-find">动态</a></li>
                <li><a href="#page3" class="ui-btn ui-btn-icon-top ui-icon-kid-user-outline">我</a></li>
            </ul>
        </div>
    </div>
    <script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
    <script charset="utf-8" src="http://map.qq.com/api/js?v=2.exp"></script>
    <script>
        $(document).on("pagebeforecreate","#care",function(){
            var pics = new Array();
            $("#lTest2").append("ok!");
            $.ajax({
                url:"careRecords.php",
                type:"POST",
                data:{
                    id:userid
                },
                success: function(data){
                    /*var obj = JSON.parse(data);
                    // alert(obj.parentname[0]);
                    var len = obj.words.length;
                    pics = obj.pictures;
                    var show = "";
                    for(var i = 0; i < len; i++){
                        //alert(obj.words[i]);
                        var start = '<li><img src="'+obj.userhead[i]+'" height="60" width="60">';
                        var mid = '<p>'+obj.words[i]+'</p>';
                        var pic = "";
                        for(var j = 0; j < pics[i].length; j++){
                            pic += '<img src="'+pics[i][j]+'" height="80" width="80">';
                        }
                        var end = '</li>';
                        show = start+mid+pic+end;
                        //show = start+mid+end;
                        var list = document.getElementById("careRecords");
                        list.innerHTML += show;
                    }
                    $('#careRecords').listview('refresh');*/
                },
                error:function(){

                }
            });

        });
    </script>
</div>





<div data-role="page" id="page3" data-theme="b">

    <script>
        $(document).on("pagecreate",function(){
            document.getElementById("username").innerHTML=username;
            $("#headimg").attr("src",headimgurl);
        });
    </script>
    <div data-role="content">
        <ul data-role="listview">
            <li data-role="list-divider"></li>
            <li>
                  <a>
                    <img style="border:10px solid white" id="headimg" src="kid2.jpg" height="80" width="80">
                      <h2 id="username"></h2>
                </a>
            </li> 
            <li data-role="list-divider"></li>
            <li><a href="#"><img src="icons/iconfont-child64.png" alt="MyKids" class="ui-li-icon ui-corner-none">我的孩子</a></li>
            <li><a href="#"><img src="icons/iconfont-shixin64.png" alt="OtherKids" class="ui-li-icon ui-corner-none">我关心的孩子</a></li>
            <li><a href="#"><img src="icons/iconfont-home64.png" alt="Family" class="ui-li-icon ui-corner-none">家人</a></li>
            <li data-role="list-divider"></li>
            <li><a href="#"><img src="icons/iconfont-settings64.png" alt="Settings" class="ui-li-icon ui-corner-none">设置</a></li>
            <li data-role="list-divider"></li>
            <li><a href="#"><img src="icons/iconfont-album64.png" alt="Albums" class="ui-li-icon ui-corner-none">影集定制</a></li>
        </ul>
    </div>

    <div data-role="footer" data-position="fixed">
        <div data-role="navbar">
            <ul>
                <li><a href="#page1" class="ui-btn ui-btn-icon-top ui-icon-kid-paw-outline">足迹</a></li>
                <li><a href="#page2" class="ui-btn ui-btn-icon-top ui-icon-kid-find-outline">动态</a></li>
                <li><a href="#page3" class="ui-btn ui-btn-icon-top ui-icon-kid-user">我</a></li>
            </ul>
        </div>
    </div>
</div>
</body>
</html>