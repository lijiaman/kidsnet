<?php
/**
 * Created by PhpStorm.
 * User: lijiaman
 * Date: 2016/3/12
 * Time: 17:44
 */
require_once 'lib/common.func.php';
require_once 'lib/weixin.class.php';
require_once 'model/SendMsgDB.php';
header("Content-type:text/html;charset=utf-8");

$kidid = $_POST["id"];
$mysql = new SaeMysql();
$sql = "select * from Records where kidID = '$kidid' order by id desc";
$records = $mysql->getData($sql);
//print_r($records);

$words = array();
$userName = array();
$userHead = array();
$albumID = array();
$imgarr = array();
$comments = array();
$likes = array();
$likeornot = array();
$date = array();

foreach($records as $record){
    $words[] = $record["words"];
    $date[] = $record["when"];
    $albumID[] = $record["id"];
    $parentID = $record["parentID"];
    $sql = "select * from Users where id = '$parentID'";
    $user = $mysql->getLine($sql);
    $userName[] = $user["username"];
    $userHead[] = $user["headimgurl"];
}
foreach($albumID as $item){
    $pics = array();
    $sql = "select * from Pictures where albumID = '$item'";
    $pictures = $mysql->getData($sql);
    if(!empty($pictures)){
        foreach($pictures as $pic){
            $pics[] = $pic["imgUrl"];
        }
    }
    $imgarr[] = $pics;
    $sql = "select * from Records where id = '$item'";
    $rec = $mysql->getLine($sql);
    $commentcnt = $rec["commentCnt"];
    $likecnt = $rec["likeCnt"];
    $comments[] = $commentcnt;
    $likes[] = $likecnt;
    $sql = "select * from Likes where recordID = '$item' and userID = '$userid'";
    $like = $mysql->getLine($sql);
    if($like == NULL){
        $likeornot[] = false;
    }else{
        $likeornot[] = true;
    }
}
$mysql->closeDb();
//print_r($userName);

$ret = array(
    "words"=>$words,
    "dates"=>$date,
    "albumurl"=>$albumID,
    "parentname"=>$userName,
    "userhead"=>$userHead,
    "pictures"=>$imgarr,
    "commentcnt"=>$comments,
    "likecnt"=>$likes,
    "likeornot"=>$likeornot
);
$jsonret = json_encode($ret);
echo $jsonret;
//print_r($imgarr);
