<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width，initial-scale=1.0">
    <title></title>
    <link rel="stylesheet" href="themes/red.min.css" />
    <link rel="stylesheet" href="themes/jquery.mobile.icons.min.css" />
    <!--<link type="text/css" rel="stylesheet" href="jquery.mobile-1.4.5.min.css">-->
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile.structure-1.4.5.min.css" />
    <link type="text/css" rel="stylesheet" href="style.css">
    <script src="jquery-2.2.0.min.js"></script>
    <script src="jquery.mobile-1.4.5.min.js"></script>
    <script>var userid = <?php echo $_GET["id"];?> ; </script>
</head>
<body>
<script>
    var username;
    var headimgurl;
    $.ajax({
        url:"echo.php",
        type:"POST",
        data:{id:userid},
        success: function (data) {
            var obj = JSON.parse(data);
            username = obj.username;
            headimgurl = obj.headimgurl;
        },
        error:function(){

        }
    });
</script>



<div data-role="page" id="page1" data-theme="b">
    <script>
        var kidname;
        var kidsex;
        var birth;
        var headimg;
        $(document).on("pageinit",function(){
            $.ajax({
                url:"getAlbums.php",
                type:"POST",
                data:{id:userid},
                success: function (data) {
                    var obj = JSON.parse(data);
                    kidname = obj.kidname;
                    kidsex = obj.kidsex;
                    birth = obj.kidbirth;
                    headimg = obj.headimgurl;
                    document.getElementById("kid-name").innerHTML=kidname;
                },
                error:function(){

                }
            })
        });
    </script>
    <div data-role="panel" id="kids" data-position="left" data-display="overlay" class="listBackground"
         data-options="onBeforeOpen:function(){
         return false;}">
        <ul data-role="listview">
            <li><a href="#" class="listBackground">孩子1号</a></li>
            <li><a href="#page1-create" class="listBackground">创建孩子</a></li>
            <li><a href="#page1-invite" class="listBackground">加入孩子</a></li>
        </ul>
    </div>
    <div data-role="header" data-theme="a" data-position="fixed">
        <a href="#kids" class="ui-btn ui-shadow ui-corner-all ui-icon-left-kids ui-btn-icon-notext">选择孩子</a>
        <h1 id="kid-name"><script>kidname</script></h1>
        <a href="#page1-edit" class="ui-btn ui-shadow ui-corner-all ui-icon-right-edit ui-btn-icon-notext">记录</a>
    </div>
    <div class="imgcenter topBackground" width="100%">
        <img src="kid2.jpg" width="50px" height="50px" class="topPicture" id="kidheadimg">
    </div>
    <div data-role="content" style="position: absolute;width: 100%;height: -webkit-calc(100% - 44px * 2)">
        <ul data-role="listview" data-inset="true">
            <li>
                  <img src="kid2.jpg" height="80" width="80">
                  <h2>KidsNet</h2>
                <p>好玩！！</p>
                <p>
                    <img src="kid2.jpg" height="80" width="80">
                    <img src="kid2.jpg" height="80" width="80">
                </p>
            </li>   
        </ul>
    </div>

    <div data-role="footer" data-position="fixed">
        <div data-role="navbar">
            <ul>
                <li><a href="#page1" class="ui-btn ui-btn-icon-top ui-icon-kid-paw">足迹</a></li>
                <li><a href="#page2" class="ui-btn ui-btn-icon-top ui-icon-kid-find-outline">动态</a></li>
                <li><a href="#page3" class="ui-btn ui-btn-icon-top ui-icon-kid-user-outline">我</a></li>
            </ul>
        </div>
    </div>
</div>

<div data-role="page" id="page1-create">
    <div data-role="header">
        <a href="#page1" class="ui-btn ui-icon-back ui-corner-all ui-btn-icon-left ui-btn-icon-notext">返回</a>
        <h1>创建孩子</h1>
    </div>
    <div data-role="content">
        <form id="createform" name="createform">
            <label for="kidname">孩子名</label>
            <input type="text" name="kidname" id="kidname">
            <fieldset data-role="controlgroup">
                <legend>性别</legend>
                <label for="boy">男孩</label>
                <input type="radio" name="gender" id="boy" value="男">
                <label for="girl">女孩</label>
                <input type="radio" name="gender" id="girl" value="女">
            </fieldset>
            <label for="birth">出生日期</label>
            <input type="date" name="birth" id="birth">
            <input type="submit" id="createbtn" name="createbtn" value="完成"/>
        </form>
    </div>
    <script>
        $("#createbtn").on("tap",function(){
            var frmdata = $("#createform").serialize();
            $.ajax({
                type:"POST",
                cache:false,
                url:"create.php",
                data:frmdata,
                success:function(data){
                    location.href = "index.php";
                },
                error:function(){

                }
            })
        });
    </script>
</div>

<div data-role="page" id="page1-invite">
    <div data-role="header">
        <a href="#page1" class="ui-btn ui-icon-back ui-corner-all ui-btn-icon-left ui-btn-icon-notext">返回</a>
        <h1>邀请家人</h1>
    </div>
    <div data-role="content">
        <label for="invitecode">请输入邀请码</label>
        <input type="text" name="invite-code" id="invite-code">
        <input type="submit" id="invite-btn" name="invite-btn" value="完成"/>
    </div>

    <script>
        $("#invite-btn").on("tap",function(){
            var code = $("#invite-code").val();
            $.ajax({
                type:"POST",
                cache:false,
                url:"invite.php",
                data:{parentid:userid,invitecode:code},
                success:function(data){
                    location.href = "index.php";
                },
                error:function(){

                }
            })
        });
    </script>
</div>

<div data-role="page" id="page1-edit">
    <?php
    //设置报错级别，忽略警告，设置字符
    error_reporting(E_ALL || ~E_NOTICE);
    header("Content-type:text/html; charset=utf-8");
    require_once "jssdk.php";
    $jssdk = new JSSDK("wxdd0345e9c31b9a00", "d4624c36b6795d1d99dcf0547af5443d");
    $signPackage = $jssdk->GetSignPackage();
    ?>
    <div data-role="header" data-theme="a" data-position="fixed">
        <a href="#page1" class="ui-btn ui-shadow ui-corner-all ui-icon-back ui-btn-icon-notext">返回</a>
        <h1>发布日志</h1>
        <a id="finishEdit" href="#" class="ui-btn ui-shadow ui-corner-all ui-icon-finish ui-btn-icon-notext">完成</a>
    </div>
    <div data-role="content">

        <input id="words" type="textarea" name="context" maxlength="400" autofocus rows="50" placeholder="宝贝此刻正在……" required/>
        <img src="addphoto.png" id="upload">
        <ul data-role="listview" data-inset="true">
            <li><a href="#"><img src="icons/iconfont-child64.png" alt="MyKids" class="ui-li-icon ui-corner-none">发给谁</a></li>
            <li><a href="#"><img src="icons/iconfont-shixin64.png" alt="OtherKids" class="ui-li-icon ui-corner-none">谁可以看</a></li>
        </ul>
        <div id="photo"></div>


    </div>
    <script>
        $("#finishEdit").on("tap",function(){
            var words = $("#words").val();
            $.ajax({
                url:"editFinish.php",
                type:"POST",
                data:{word:words},
                success: function (data) {

                },
                error:function(){

                }
            })
        })
    </script>


    <!--    <script src="https://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>-->
    <!--    <script>-->
    <!--        wx.config({-->
    <!--            debug: true, //调试阶段建议开启-->
    <!--            appId: '--><?php //echo $signPackage["appId"];?>//',
    //            timestamp: <?php //echo $signPackage["timestamp"];?>//,
    //            nonceStr: '<?php //echo $signPackage["nonceStr"];?>//',
    //            signature: '<?php //echo $signPackage["signature"];?>//',
    //            jsApiList: [
    //                /*
    //                 * 所有要调用的 API 都要加到这个列表中
    //                 * 这里以图像接口为例
    //                 */
    //                "chooseImage",
    //                "previewImage",
    //                "uploadImage",
    //                "downloadImage"
    //            ]
    //        });
    //
    //
    //        //定义images用来保存选择的本地图片ID，和上传后的服务器图片ID
    //        var images = {
    //            localId: [],
    //            serverId: []
    //        };
    //        wx.ready(function () {
    //            // 在这里调用 API
    //            $("#upload").on("tap",function(){
    //                wx.chooseImage ({
    //                    success : function(res){
    //                        images.localId = res.localIds;  //保存到images
    //                        // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
    //                        var i = 0, len = images.localId.length;
    //                        function wxUpload(){
    //                            wx.uploadImage({
    //                                localId: images.localId[i], // 需要上传的图片的本地ID，由chooseImage接口获得
    //                                isShowProgressTips: 1, // 默认为1，显示进度提示
    //                                success: function (res) {
    //                                    i++;
    //                                    //将上传成功后的serverId保存到serverid
    //                                    images.serverId.push(res.serverId);
    //                                    if(i < len){
    //                                        wxUpload();
    //                                    }
    //                                    if(i == len){
    //                                        $.ajax({
    //                                            url:"uploadimg.php",
    //                                            type:"POST",
    //                                            data:{mediaID:images.serverId},
    //                                            success: function (data) {
    //                                                $("#test").html(data);
    //                                            },
    //                                            error:function(){
    //
    //                                            }
    //                                        });
    //                                    }
    //                                }
    //                            });
    //                        }
    //                        wxUpload();
    //                    }
    //                });
    //            });
    //
    ////            var imgview = "";
    ////            for(var i in images.localId){
    ////                var imgsrc = images.localId[i];
    ////                imgview = imgview + '<img src="'+imgsrc+'"/>';
    ////            }
    ////            $("#photo").append(imgview);
    //        });
    //    </script>
    </div>


    <div data-role="page" id="page2" data-theme="b">
        <div data-role="header" data-theme="a">
        <div data-role="navbar">
        <ul>
        <li><a href="#">推荐</a></li>
        <li><a href="#">关注</a></li>
        <li><a href="#">发现</a></li>
        </ul>
        </div>
        </div>
        <div class="ui-content" role="main">
        <ul data-role="listview" data-inset="true">

        <li>
        <a href="#">
        <img src="c2.png">
        <h2>陈金金</h2>
        <p>台湾    哈娜 两岁四个月</p>
    </a>
    <img src="cc2.jpg">
        </li>

        </ul>

        </div>

        <div data-role="footer" data-position="fixed">
        <div data-role="navbar">
        <ul>
        <li><a href="#page1" class="ui-btn ui-btn-icon-top ui-icon-kid-paw-outline">足迹</a></li>
        <li><a href="#page2" class="ui-btn ui-btn-icon-top ui-icon-kid-find">动态</a></li>
        <li><a href="#page3" class="ui-btn ui-btn-icon-top ui-icon-kid-user-outline">我</a></li>
        </ul>
        </div>
        </div>
        </div>


        <div data-role="page" id="page3" data-theme="b">
        <script>
        $(document).on("pageinit",function(){
            document.getElementById("username").innerHTML=username;
            $("#headimg").attr("src",headimgurl);
        });
    </script>
    <div data-role="content">
        <ul data-role="listview" data-inset="true">
            <li>
                  <img id="headimg" src="kid2.jpg" height="80" width="80">
                  <h2 id="username"></h2>
            </li>  
        </ul>
        <ul data-role="listview" data-inset="true">
            <li><a href="#"><img src="icons/iconfont-child64.png" alt="MyKids" class="ui-li-icon ui-corner-none">我的孩子</a></li>
            <li><a href="#"><img src="icons/iconfont-shixin64.png" alt="OtherKids" class="ui-li-icon ui-corner-none">我关心的孩子</a></li>
            <li><a href="#"><img src="icons/iconfont-home64.png" alt="Family" class="ui-li-icon ui-corner-none">家人</a></li>
            <li data-role="list-divider"></li>
            <li><a href="#"><img src="icons/iconfont-settings64.png" alt="Settings" class="ui-li-icon ui-corner-none">设置</a></li>
            <li data-role="list-divider"></li>
            <li><a href="#"><img src="icons/iconfont-album64.png" alt="Albums" class="ui-li-icon ui-corner-none">影集定制</a></li>
        </ul>
    </div>

    <div data-role="footer" data-position="fixed">
        <div data-role="navbar">
            <ul>
                <li><a href="#page1" class="ui-btn ui-btn-icon-top ui-icon-kid-paw-outline">足迹</a></li>
                <li><a href="#page2" class="ui-btn ui-btn-icon-top ui-icon-kid-find-outline">动态</a></li>
                <li><a href="#page3" class="ui-btn ui-btn-icon-top ui-icon-kid-user">我</a></li>
            </ul>
        </div>
    </div>
</div>
</body>
</html>