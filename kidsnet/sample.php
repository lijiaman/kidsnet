<?php
//设置报错级别，忽略警告，设置字符
error_reporting(E_ALL || ~E_NOTICE);
header("Content-type:text/html; charset=utf-8");
require_once "jssdkforsae.php";
$jssdk = new JSSDK("wxdd0345e9c31b9a00", "d4624c36b6795d1d99dcf0547af5443d");
$signPackage = $jssdk->GetSignPackage();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!--因为在手机中，所以添加viewport-->
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title>微信测试</title>
    <script src="jquery-2.2.0.min.js"></script>
    <script src="jquery.mobile-1.4.5.min.js"></script>
</head>
<body>
<button id="weixin" style="display: block;margin: 2em auto">微信接口测试</button>

<div id="test"></div>
<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<script>
    wx.config({
        debug: true, //调试阶段建议开启
        appId: '<?php echo $signPackage["appId"];?>',
        timestamp: <?php echo $signPackage["timestamp"];?>,
        nonceStr: '<?php echo $signPackage["nonceStr"];?>',
        signature: '<?php echo $signPackage["signature"];?>',
        jsApiList: [
            /*
             * 所有要调用的 API 都要加到这个列表中
             * 这里以图像接口为例
             */
            "chooseImage",
            "previewImage",
            "uploadImage",
            "downloadImage"
        ]
    });
    var btn = document.getElementById('weixin');
    //定义images用来保存选择的本地图片ID，和上传后的服务器图片ID
    var images = {
        localId: [],
        serverId: []
    };
    wx.ready(function () {
        // 在这里调用 API
        btn.onclick = function(){
            wx.chooseImage ({
                success : function(res){
                    images.localId = res.localIds;  //保存到images
                    // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
                    var i = 0, len = images.localId.length;
                    function wxUpload(){
                        wx.uploadImage({
                            localId: images.localId[i], // 需要上传的图片的本地ID，由chooseImage接口获得
                            isShowProgressTips: 1, // 默认为1，显示进度提示
                            success: function (res) {
                                i++;
                                //将上传成功后的serverId保存到serverid
                                images.serverId.push(res.serverId);
                                if(i < len){
                                    wxUpload();
                                }
                                if(i == len){
                                    $.ajax({
                                        url:"uploadimg.php",
                                        type:"POST",
                                        data:{mediaID:images.serverId},
                                        success: function (data) {
                                            $("#test").html(data);
                                        },
                                        error:function(){

                                        }
                                    });
                                }
                            }
                        });
                    }
                    wxUpload();
                }
            });
        };

    });
</script>
</body>
</html>