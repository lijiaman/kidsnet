<?php

require_once 'lib/common.func.php';
require_once 'lib/weixin.class.php';
require_once 'model/SendMsgDB.php';
header("Content-type:text/html;charset=utf-8");

$userId = $_POST["id"];
$latitude = floatval($_POST["latitude"]);
$longitude = floatval($_POST["longitude"]);

$mysql = new SaeMysql();
//$sql = "select * from Records where latitude = 116.336";116.33567,39.97694
$sql = "select * from Records where ABS(ABS('$latitude'-latitude)+ABS('$longitude'-longitude))<0.009 and privellege = '公开'";
//$sql = "select * from Records where ABS(ABS('$latitude'-latitude)+ABS('$longitude'-longitude))<0.009";
$records = $mysql->getData($sql);

// where ABS(ABS('$latitude'-latitude)-ABS('$longitude'-longitude))<0.009

$words = array();

$userName = array();
$userHead = array();

$albumID = array();
$imgarr = array();
$date = array();
$places = array();
$comments = array();
$likes = array();
$likeornot = array();
$pids = array();

if(!empty($records)){
    foreach($records as $record)
    {
        $words[] = $record["words"];
        $date[] = $record["when"];
        $places[] = $record["location"];
        $albumID[] = $record["id"];
        $parentID = $record["parentID"];
        $pids[] = $parentID;
        $sql = "select * from Users where id = '$parentID'";
        $user = $mysql->getLine($sql);
        $userName[] = $user["username"];
        $userHead[] = $user["headimgurl"];
    }
}


foreach($albumID as $item){
    $pics = array();
    $sql = "select * from Pictures where albumID = '$item'";
    $pictures = $mysql->getData($sql);
    if(!empty($pictures)){
        foreach($pictures as $pic){
            $pics[] = $pic["imgUrl"];
        }
    }


      $imgarr[] = $pics;
      $sql = "select * from Records where id = '$item'";
      $rec = $mysql->getLine($sql);
    $commentcnt = $rec["commentCnt"];
      $likecnt = $rec["likeCnt"];
      $comments[] = $commentcnt;
      $likes[] = $likecnt;
      $sql = "select * from Likes where recordID = '$item' and userID = '$userid'";
      $like = $mysql->getLine($sql);
      if($like == NULL){
          $likeornot[] = false;
      }else{
          $likeornot[] = true;
      }
}/*/*
*/
$mysql->closeDb();
//print_r($userName);

$ret = array(
    "words"=>$words,
    "dates"=>$date,
    "places"=>$places,
    "albumid"=>$albumID,
    "pid"=>$pids,
    "parentname"=>$userName,
    "userhead"=>$userHead,
    "pictures"=>$imgarr,
    "commentcnt"=>$comments,
    "likecnt"=>$likes,
    "likeornot"=>$likeornot
);
$jsonret = json_encode($ret);
echo $jsonret;