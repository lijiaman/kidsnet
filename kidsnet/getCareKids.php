<?php
/**
 * Created by PhpStorm.
 * User: lijiaman
 * Date: 2016/3/15
 * Time: 17:04
 */
require_once 'lib/common.func.php';
require_once 'lib/weixin.class.php';
require_once 'model/SendMsgDB.php';
header("Content-type:text/html;charset=utf-8");

$userid = $_POST["id"];
$mysql = new SaeMysql();
$sql = "select * from Concerns where userID = '$userid'";
$concerns = $mysql -> getData($sql);

$pname = array();
$words = array();
$dates = array();
$places = array();
$pimg = array();
$pids = array();
$imgarr = array();
$likecnt = array();
$commentcnt = array();
$recordid = array();
$likeornot = array();

foreach($concerns as $concern){
    $kidid = $concern["kidID"];
    $sql = "select * from Records where kidID = '$kidid' and privellege <> '私密'";
    $records = $mysql -> getData($sql);
    foreach($records as $record){
        $recordid[] = $record["id"];
        $words[] = $record["words"];
        $dates[] = $record["when"];
        $places[] = $record["location"];
        $likecnt[] = $record["likeCnt"];
        $commentcnt[] = $record["commentCnt"];
        $albumid = $record["id"];
        $sql = "select * from Likes where recordID = '$albumid' and userID = '$userid'";
        $like = $mysql->getLine($sql);
        if($like == NULL){
            $likeornot[] = false;
        }else{
            $likeornot[] = true;
        }
        $sql = "select * from Pictures where albumID = '$albumid'";
        $pictures = $mysql->getData($sql);
        $img = array();
        if(!empty($pictures)){
            foreach($pictures as $picture){
                $picurl = $picture["imgUrl"];
                $img[] = $picurl;
            }
        }


        $imgarr[] = $img;
        $pid = $record["parentID"];
        $sql = "select * from Users where id = '$pid'";
        $user = $mysql -> getLine($sql);
        $pids[] = $user["id"];
        $pname[] = $user["username"];
        $pimg[] = $user["headimgurl"];
    }

}

$mysql -> closeDb();
$ret = array(
    "words"=>$words,
    "dates"=>$dates,
    "places"=>$places,
    "names"=>$pname,
    "imgs"=>$pimg,
    "albums"=>$imgarr,
    "ids"=>$pids,
    "likes"=>$likecnt,
    "commentcnt"=>$commentcnt,
    "recordids"=>$recordid,
    "likeornot"=>$likeornot

);
$jsonret = json_encode($ret);
echo $jsonret;