<?php
/**
 * Created by PhpStorm.
 * User: lijiaman
 * Date: 2016/2/29
 * Time: 17:52
 */
require_once 'lib/common.func.php';
require_once 'lib/weixin.class.php';
require_once 'model/SendMsgDB.php';
header("Content-type:text/html;charset=utf-8");

$userid = $_POST["id"];

$mysql = new SaeMysql();
$sql = "select * from Users where id = '$userid'";
$user = $mysql->getLine($sql);
//从数据库里面依据用户id 获取用户名和 用户头像url
$username = $user["username"];
$headimgurl = $user["headimgurl"];

$mysql->closeDb();

$ret = array(
    "username"=>$username,
    "headimgurl"=>$headimgurl
);
//数据包返回到前端界面
$jsonret = json_encode($ret);
echo $jsonret;

