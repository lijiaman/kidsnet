<?php
/**
 * Created by PhpStorm.
 * User: lijiaman
 * Date: 2016/3/14
 * Time: 20:07
 */
require_once 'lib/common.func.php';
require_once 'lib/weixin.class.php';
require_once 'model/SendMsgDB.php';
header("Content-type:text/html;charset=utf-8");

$recordid = $_POST["id"];

$mysql = new SaeMysql();
$sql = "select * from Comments where recordID = '$recordid'";
$comments = $mysql -> getData($sql);

$words = array();
$times = array();
$names = array();
$headimgs = array();
foreach($comments as $comment){
    $userid = $comment["userID"];
    $word = $comment["comment"];
    $time = $comment["time"];
    $sql = "select * from Users where id = '$userid'";
    $user = $mysql->getLine($sql);
    $username = $user["username"];
    $headimg = $user["headimgurl"];
    $words[] = $word;
    $times[] = $time;
    $names[] = $username;
    $headimgs[] = $headimg;
}

$ret = array(
    "words"=>$words,
    "times"=>$times,
    "names"=>$names,
    "headimgs"=>$headimgs

);

$jsonret = json_encode($ret);
echo $jsonret;