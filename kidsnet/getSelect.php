<?php
/**
 * Created by PhpStorm.
 * User: lijiaman
 * Date: 2016/3/14
 * Time: 9:55
 */
require_once 'lib/common.func.php';
require_once 'lib/weixin.class.php';
require_once 'model/SendMsgDB.php';
header("Content-type:text/html;charset=utf-8");

$kidid = $_POST["id"];
$mysql = new SaeMysql();
$sql = "select * from Kids where id = '$kidid'";
$kid = $mysql->getLine($sql);

$kidname = $kid["kidname"];
$kidimg = $kid["headimg"];

$mysql->closeDb();

$ret = array(
    "kidname"=>$kidname,
    "kidheadimg"=>$kidimg
);
$jsonret = json_encode($ret);
echo $jsonret;